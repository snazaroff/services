package com.corporate.api.service;

import com.corporate.api.security.policy.BasicPolicyEnforcement;
import lombok.extern.slf4j.Slf4j;
import org.casbin.jcasbin.main.Enforcer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URL;

@Slf4j
@Service
public class IdentifyService {

    @Autowired
    BasicPolicyEnforcement basicPolicyEnforcement;

    public IdentifyService() {}

    public boolean identify(Object user, Object data, String action) {
        log.debug("identify(user: {}, data: {}, action: {})", user, data, action);

        return basicPolicyEnforcement.check(user, data, action, null);
    }
}