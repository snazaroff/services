package com.corporate.api.model.proxy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
@AllArgsConstructor
public class OrderItem implements Serializable {

    private Long orderItemId;
    private Long productId;
    private Integer amount;
    private Integer price;
}


