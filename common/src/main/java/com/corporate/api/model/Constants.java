package com.corporate.api.model;

public class Constants {

    public static final String CUSTOMER_RESERVE_OK_QUEUE = "customer-reserve-ok-queue";
    public static final String CUSTOMER_RESERVE_REJECT_QUEUE = "customer-reserve-reject-queue";

    public static final String ORDER_RESERVE_CONFIRM_QUEUE = "order-reserve-confirm-queue";
    public static final String ORDER_RESERVE_REJECT_QUEUE = "order-reserve-reject-queue";
    public static final String ORDER_RESERVE_OK_QUEUE = "order-reserve-ok-queue";
    public static final String ORDER_RESERVE_TOPIC = "order-reserve-topic";
//    public static final String ORDER_RESERVE_REJECT_QUEUE = "order-reserve-reject-queue";
    public static final String ORDER_RESPONSE_QUEUE = "order-queue";

    public static final String PRODUCT_RESERVE_OK_QUEUE = "product-reserve-ok-queue";
    public static final String PRODUCT_RESERVE_REJECT_QUEUE = "product-reserve-reject-queue";

    public static final String REPORT_QUEUE = "report-queue";
}