package com.corporate.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class WrongRegistrationCodeException extends Exception {
    public WrongRegistrationCodeException(String phone, String code) {
        super(format("Wrong registration code '%s' for phone '%s'", code, phone));
    }
}
