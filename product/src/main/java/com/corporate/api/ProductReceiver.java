package com.corporate.api;

import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;

@Slf4j
@Component
public class ProductReceiver {

    @Autowired
    ProductSender productSender;

    @Autowired
    ProductService productService;

/*
    @JmsListener(destination = Constants.ORDER_RESERVE_TOPIC)
    public void receiveReserveMessage(final Message<Order> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveReserveMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        Order order = message.getPayload();

        ProductReserveResult productReserveResult = productService.reserve(order);
        if(productReserveResult.getStatus() == ProductReserveStatus.RESERVED)
            productSender.sendProductReserveOk(productReserveResult);
        else
            productSender.sendProductReserveReject(productReserveResult);
    }
*/
    @JmsListener(destination = Constants.ORDER_RESERVE_TOPIC, containerFactory = "jmsListenerContainerFactory")
    public void receiveOrderReserveMessage(final Message<Order> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveOrderReserveMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        Order order = message.getPayload();

        ProductReserveResult productReserveResult = productService.reserve(order);
        if(productReserveResult.getStatus() == ProductReserveStatus.RESERVED)
            productSender.sendProductReserveOk(productReserveResult);
        else
            productSender.sendProductReserveReject(productReserveResult);
    }

    @JmsListener(destination = Constants.ORDER_RESERVE_CONFIRM_QUEUE)
    public void receiveConfirmReserveMessage(final Message<ConfirmReserveOrder> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveConfirmReserveMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        ConfirmReserveOrder confirmReserve = message.getPayload();
        productService.confirmReserve(confirmReserve);
    }

    @JmsListener(destination = Constants.ORDER_RESERVE_REJECT_QUEUE)
    public void receiveRejectReserveMessage(final Message<RejectReserveOrder> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveRejectReserveMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        RejectReserveOrder rejectReserve = message.getPayload();
        productService.rejectReserve(rejectReserve);
    }
}