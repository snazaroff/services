package com.corporate.api.model;

public enum CustomerReserveStatus {

    RESERVED(0),
    REJECTED(1),
    CONFIRMED(2);

    private int statusId;

    CustomerReserveStatus(int statusId) {
        this.statusId = statusId;
    }
}
