package com.corporate.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ToString(exclude = {"order"})
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name="order_item")
public class OrderItem implements Serializable {

    public OrderItem() { }

    @Id
    @Column(name = "order_item_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderItemId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @NotNull(message = "Product cannot be null")
    @Column(name="product_id", nullable=false, updatable = false)
    private Long productId;

    @Column(name = "price")
    private Integer price;

    @Column(name = "amount")
    private Integer amount;

    public com.corporate.api.model.proxy.OrderItem toProxy() {
        com.corporate.api.model.proxy.OrderItem orderItem =
                new com.corporate.api.model.proxy.OrderItem(this.getOrderItemId(), this.getProductId(), this.getAmount(), this.getPrice());
        return orderItem;
    }
}