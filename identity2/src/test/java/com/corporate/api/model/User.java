package com.corporate.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class User implements Comparable<User> {
    String name;
    Department department;
    Role role;
    User boss;

    public User(String name, Department department, Role role) {
        this.name = name;
        this.department = department;
        this.role = role;
    }

    @Override
    public int compareTo(User o) {
        return this.name.compareTo(o.name);
    }
}