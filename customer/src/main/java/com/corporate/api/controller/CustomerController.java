package com.corporate.api.controller;

import com.corporate.api.model.*;
import com.corporate.api.service.CustomerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/customer")
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<Customer>> getAll() {
        log.debug("ProductController.getAll()");

        List<Customer> customers = customerService.getAll();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @GetMapping(path = "/{customerId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Customer> get(@PathVariable Long customerId) {
        log.debug("ProductController.info(productId: {})", customerId);

        Customer customer = customerService.getById(customerId);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @PostMapping(path = "/confirm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Void> confirmReserve(@RequestBody @Valid ConfirmReserveOrder confirmReserve) {
        log.debug("CustomerController.confirmReserve(confirmReserve: {})", confirmReserve);

        customerService.confirmReserve(confirmReserve);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/reject", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Void> rejectReserve(@RequestBody @Valid RejectReserveOrder rejectReserve) {
        log.debug("CustomerController.rejectReserve(confirmReserve: {})", rejectReserve);

        customerService.rejectReserve(rejectReserve);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}