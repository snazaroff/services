package com.corporate.api.controller;

import com.corporate.api.model.Order;
import com.corporate.api.service.OrderService;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/order")
@AllArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<Order>> getAll() {
        log.debug("OrderController.getAll()");

        List<Order> orders = orderService.getAll();
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(path = "/{orderId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Order> getOrder(@ApiParam(value = "Идентификатор ордера") @PathVariable Long orderId) {
        log.debug("OrderController.getOrder(orderId : {})", orderId);

        Order order = orderService.getById(orderId);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }


    @PostMapping(path = "/reserve", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Order> reserveOrder(@PathVariable Order order) {
        log.debug("OrderController.reserveOrder(order: {})", order);

        orderService.reserve(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }


//    @GetMapping(path = "/info/{phone}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<String> info(@ApiParam(value = "Телефон") @PathVariable String phone,
//                             HttpServletRequest request) {
//        log.debug("CustomerController.info(phone : {}", phone);
//
//        return new ResponseEntity<>("", HttpStatus.RESERVED_OK);
//    }
}