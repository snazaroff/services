package com.corporate.api.model;

public enum IssueStatus {
	NEW, ASSIGNED, COMPLETED
}
