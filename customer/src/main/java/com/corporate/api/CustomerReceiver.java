package com.corporate.api;

import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;

@Slf4j
@Component
public class CustomerReceiver {

    @Autowired
    CustomerSender customerSender;

    @Autowired
    CustomerService customerService;

    @JmsListener(destination = Constants.ORDER_RESERVE_TOPIC, containerFactory = "jmsListenerContainerFactory")
    public void receiveReserveMessage(final Message<Order> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveReserveMessage(message : {}", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        Order order = message.getPayload();
        CustomerReserveResult reserveResults = customerService.reserve(order);
        if(reserveResults.getCustomerReserveStatus() == CustomerReserveStatus.RESERVED)
            customerSender.sendCustomerReserveOk(reserveResults);
        else
            customerSender.sendCustomerReserveReject(reserveResults);
    }

    @JmsListener(destination = Constants.ORDER_RESERVE_CONFIRM_QUEUE)
    public void receiveConfirmReserveMessage(final Message<ConfirmReserveOrder> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveConfirmReserveMessage(message : {}", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        ConfirmReserveOrder confirmReserve = message.getPayload();
        customerService.confirmReserve(confirmReserve);
    }

    @JmsListener(destination = Constants.ORDER_RESERVE_REJECT_QUEUE)
    public void receiveRejectReserveMessage(final Message<RejectReserveOrder> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveRejectReserveMessage(message : {}", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        RejectReserveOrder rejectReserve = message.getPayload();
        customerService.rejectReserve(rejectReserve);
    }
}