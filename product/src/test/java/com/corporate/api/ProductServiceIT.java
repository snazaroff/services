package com.corporate.api;

import com.corporate.api.model.*;
import com.corporate.api.model.Product;
import com.corporate.api.model.proxy.*;
import com.corporate.api.repository.*;
import com.corporate.api.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class ProductServiceIT {

    private MockMvc mockMvc;

    private ObjectMapper om = new ObjectMapper();

    private final Customer[] customers = {new Customer(1L, "Волобуев", 30), new Customer(2L, "Прошкин", 1000)};

    @Autowired
    ProductService productService;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductReserveRepository reserveRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }


    @Test
//    @Ignore
    public void testRegistration() throws Exception {
        log.debug("testRegistration()");

        List<Product> products = productService.getAll();
        log.debug("products: {}", products);

        Long orderId = 1L;

        Customer customer = customers[0];
        Order order = new Order(orderId, customer.getCustomerId());
        order.setCustomerId(customer.getCustomerId());
        for(Product product: products) {
            OrderItem orderItem = new OrderItem(product.getProductId(), product.getProductId(), 3, product.getPrice());
//            orderItem.setProductId(product.getProductId());
//            orderItem.setPrice(product.getPrice());
//            orderItem.setAmount(100);
//            orderItem.setOrder(order);
            order.getOrderItems().add(orderItem);
        }



        List<ProductReserve> reserves = new ArrayList<>();
        ProductReserve reserve = new ProductReserve();
        reserve.setOrderId(orderId);
        reserve.setProductId(products.get(0).getProductId());
        reserve.setAmount(1);
        reserve.setStart(LocalDateTime.now());
        reserve.setStatus(ProductReserveStatus.RESERVED);

        reserves.add(reserve);
        ProductReserveResult productReserveResult = productService.reserve(order);
        log.debug("productReserveResult: {}", productReserveResult);

        products = productService.getAll();
        log.debug("products: {}", products);

        reserves = reserveRepository.findAll();
        log.debug("reserves: {}", reserves);

        productService.rejectReserve(new RejectReserveOrder(orderId));

        products = productService.getAll();
        log.debug("products: {}", products);

        reserves = reserveRepository.findAll();
        log.debug("reserves: {}", reserves);
    }
}