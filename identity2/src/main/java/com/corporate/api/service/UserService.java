package com.corporate.api.service;

import java.util.List;

import com.corporate.api.model.ProjectUser;

public interface UserService {
	ProjectUser findUserByName(String name);
	List<ProjectUser> findUserByProject(Integer projectId);
}
