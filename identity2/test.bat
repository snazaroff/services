del *.log.*
del *.out.*
del *.err.*

mvn clean package -Dtest=IdentityServiceIT > test.out 2>test.err