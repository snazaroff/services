package com.corporate.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name="money_reserve")
public class ReserveMoney implements Serializable {

    @Id
    @Column(name = "reserve_money_id")
    @JsonProperty("reserve_money_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reserveMoneyId;
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "order_id")
    private Long orderId;
    @Column(name = "summa")
    private Integer summa = 0;
    @Column(name = "start_date_time")
    private LocalDateTime start;
    @Column(name = "status")
    private ReserveMoneyStatus status;

    public ReserveMoney() { }

//    public ReserveMoney(ProductReserveResult reserveResult) {
//        this.orderId = reserveResult.getOrderId();
//        this.productId = reserveResult.getProductId();
//        this.amount = reserveResult.getOrderedAmount();
//        this.start = LocalDateTime.now();
//        this.status = ProductReserveStatus.RESERVED;
//    }
}
