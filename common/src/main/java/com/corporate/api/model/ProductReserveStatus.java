package com.corporate.api.model;

public enum ProductReserveStatus {

    RESERVED(0),
    REJECTED(1),
    CONFIRMED(2);

    private int statusId;

    ProductReserveStatus(int statusId) {
        this.statusId = statusId;
    }
}
