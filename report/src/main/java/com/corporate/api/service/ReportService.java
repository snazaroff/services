package com.corporate.api.service;

import com.corporate.api.configuration.LoggerConfig;
import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.model.proxy.OrderItem;
import com.corporate.api.repository.ReportRepository;
import com.corporate.api.repository.ProductReserveRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReportService {

    private LoggerService log;

    private final ReportRepository reportRepository;
    private final ProductReserveRepository reserveRepository;

    public ReportService(
            ReportRepository productRepository,
            ProductReserveRepository reserveRepository,
            LoggerConfig loggerConfig
            ) {
        this.reportRepository = productRepository;
        this.reserveRepository = reserveRepository;
        log = new LoggerService(ReportService.class, loggerConfig);
    }

    public List<Product> getAll() {
        return reportRepository.findAll();
    }

    public Product getById(Long productId) {
        return reportRepository.getOne(productId);
    }

    @Transactional
    public void add(String message) {
        log.debug("add(message: {})", message);
//
//        ProductReserveResult result = new ProductReserveResult();
//        result.setOrderId(order.getOrderId());
//
//        for(OrderItem orderItem: order.getOrderItems()) {
//            Product product = reportRepository.getOne(orderItem.getProductId());
//
//            ProductReserveItem productReserveItem = new ProductReserveItem();
//            productReserveItem.setAvailableAmount(product.getAmount());
//            productReserveItem.setOrderedAmount(orderItem.getAmount());
//            productReserveItem.setProductId(product.getProductId());
//
//            result.getProductReserveItems().add(productReserveItem);
//        }
//        //Проверяем наличие достаточного кол-ва товара
//        if(result.isOk()) {
//            //Сохраняем
//            result.setStatus(ProductReserveStatus.RESERVED);
//            for(ProductReserveItem productReserveItem: result.getProductReserveItems()) {
//                ProductReserve reserveProduct = new ProductReserve();
//                reserveProduct.setReserveId(0L);
//                reserveProduct.setOrderId(order.getOrderId());
//                reserveProduct.setProductId(productReserveItem.getProductId());
//                reserveProduct.setAmount(productReserveItem.getOrderedAmount());
//                reserveProduct.setStart(LocalDateTime.now());
//                reserveProduct.setStatus(ProductReserveStatus.RESERVED);
//
//                Product product = reportRepository.getOne(reserveProduct.getProductId());
//                if(product.getAmount() >= productReserveItem.getOrderedAmount()) {
//                    product.setAmount(product.getAmount() - productReserveItem.getOrderedAmount());
//                    reportRepository.save(product);
//                }
//                reserveRepository.save(reserveProduct);
//            }
//
//        } else {
//            result.setStatus(ProductReserveStatus.REJECTED);
//        }
//        return result;
    }
}