package com.corporate.api.product.model;

import com.corporate.api.model.ProductReserveStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductReserve implements Serializable {

    private Long reserveId;
    private Long orderId;
    private Long productId;
    private Integer amount;
    private LocalDateTime start;
    private ProductReserveStatus status;

    public ProductReserve() { }
}
