package com.corporate.api.repository;

import com.corporate.api.model.ProductReserve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductReserveRepository extends JpaRepository<ProductReserve, Long> {
    List<ProductReserve> findByOrderId(Long orderid);
}