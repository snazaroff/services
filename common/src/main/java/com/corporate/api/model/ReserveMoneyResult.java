package com.corporate.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class ReserveMoneyResult implements Serializable {
    private Long orderId;
    private Long customerId;
    private Integer orderedSumma;
    private Integer availableSumma;
}
