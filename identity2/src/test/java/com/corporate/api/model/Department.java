package com.corporate.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Department implements Comparable<Department> {
    String name;

    public Department(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Department o) {
        return this.name.compareTo(o.name);
    }
}