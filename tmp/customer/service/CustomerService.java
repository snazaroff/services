package com.corporate.api.customer.service;

//import com.corporate.api.model.*;
import com.corporate.api.customer.model.ReserveMoney;
import com.corporate.api.customer.repository.CustomerRepository;
import com.corporate.api.customer.repository.MoneyReserveRepository;
import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.model.proxy.OrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final MoneyReserveRepository reserveRepository;

        public CustomerService(
            CustomerRepository productRepository,
            MoneyReserveRepository reserveRepository) {
        this.customerRepository = productRepository;
        this.reserveRepository = reserveRepository;
    }

    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Customer getById(Long productId) {
        return customerRepository.getOne(productId);
    }

    @Transactional
    public CustomerReserveResult reserve(Order order) {
        log.debug("reserve(order: {})", order);

        int reserveSumma = 0;

        //Считаем сумму по всему заказу
        for(OrderItem orderItem: order.getOrderItems()) {
            reserveSumma = reserveSumma + orderItem.getAmount()*orderItem.getPrice();
        }

        Customer customer = customerRepository.getOne(order.getCustomerId());

        CustomerReserveResult customerReserveResult = new CustomerReserveResult();
        customerReserveResult.setOrderId(order.getOrderId());
        customerReserveResult.setCustomerId(order.getCustomerId());
        customerReserveResult.setAvailableSumma(customer.getDeposit());
        customerReserveResult.setOrderedSumma(reserveSumma);

        if (customer.getDeposit() >= reserveSumma) {
            //Если сумма депозита не меньше суммы заказа
            //резервируем сумму

            customerReserveResult.setCustomerReserveStatus(CustomerReserveStatus.RESERVED);
            ReserveMoney reserveMoney = new ReserveMoney();
            reserveMoney.setOrderId(order.getOrderId());
            reserveMoney.setCustomerId(order.getCustomerId());
            reserveMoney.setStatus(ReserveMoneyStatus.RESERVED);
            reserveMoney.setStart(LocalDateTime.now());
            reserveMoney.setStatus(ReserveMoneyStatus.RESERVED);
            reserveMoney.setSumma(reserveSumma);

            reserveRepository.save(reserveMoney);

            customer.setDeposit(customer.getDeposit() - reserveSumma);
            customerRepository.save(customer);
        } else {
            customerReserveResult.setCustomerReserveStatus(CustomerReserveStatus.REJECTED);
        }

        return customerReserveResult;
    }

    @Transactional
    public void confirmReserve(@Valid ConfirmReserveOrder confirmReserve) {
        log.debug("confirmReserve(confirmReserve: {})", confirmReserve);
        List<ReserveMoney> reserves = reserveRepository.findByOrderId(confirmReserve.getOrderId());
        for(ReserveMoney reserve: reserves) {
            reserve.setStatus(ReserveMoneyStatus.CONFIRMED);
        }
        reserveRepository.saveAll(reserves);
    }

    @Transactional
    public void rejectReserve(@Valid RejectReserveOrder rejectReserve) {
        log.debug("rejectReserve(rejectReserve: {})", rejectReserve);
        List<ReserveMoney> reserves = reserveRepository.findByOrderId(rejectReserve.getOrderId());
        for(ReserveMoney reserve: reserves) {
            reserve.setStatus(ReserveMoneyStatus.REJECTED);
            Customer customer = customerRepository.getOne(reserve.getCustomerId());
            customer.setDeposit(customer.getDeposit() + reserve.getSumma());
            customerRepository.save(customer);
        }
        reserveRepository.saveAll(reserves);
    }
}