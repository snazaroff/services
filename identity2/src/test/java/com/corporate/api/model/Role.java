package com.corporate.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Role {
    String name;

    public Role(String name) {
        this.name = name;
    }
}
