package com.corporate.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name="orders")
public class Order implements Serializable {

    public Order() {
        initDateTime = LocalDateTime.now();
    }

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    @Column(name = "customer_id")
    private Long customerId;

    @JsonProperty("create_date")
    @Column(name = "create_date")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime initDateTime;

    @Column(name = "status")
    private Integer status;

    @OneToMany(mappedBy = "order", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)
    Set<OrderItem> orderItems = new HashSet<>();

    public com.corporate.api.model.proxy.Order toProxy() {
        com.corporate.api.model.proxy.Order order = new com.corporate.api.model.proxy.Order(this.getOrderId(), this.getCustomerId());
        for(OrderItem orderItem: this.orderItems) {
            log.debug("orderItem: {}", orderItem);
            log.debug("orderItem.toProxy(): {}", orderItem.toProxy());

            order.getOrderItems().add(orderItem.toProxy());
        }
        return order;
    }

    public void setStatus(OrderStatus status) {
        this.status = status.ordinal();
    }

    public void setStatus(int status) {
        this.status = status;
    }

}