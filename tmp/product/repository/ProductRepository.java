package com.corporate.api.product.repository;

import com.corporate.api.model.Product;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class ProductRepository {

    private final Product[] products = {
            new Product(1L, "Утюг", 10, 150),
            new Product(2L, "Кирпич", 5, 1000)
    };

    public List<Product> findAll() {
        return Arrays.asList(products);
    }

    public Product getOne(Long productId) {
        if(productId == null || productId.equals(""))
            return null;
        for(Product product: products) {
            if(productId.equals(product.getProductId()))
                return product;
        }
        return null;
    }

    public void save(Product product) {
        Product oldProduct = getOne(product.getProductId());
        if(oldProduct != null) {
            oldProduct.setName(product.getName());
            oldProduct.setAmount(product.getAmount());
            oldProduct.setPrice(product.getPrice());
        }
    }
}