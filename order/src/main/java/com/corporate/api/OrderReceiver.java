package com.corporate.api;

import javax.jms.Session;

import com.corporate.api.model.Constants;
import com.corporate.api.model.CustomerReserveResult;
import com.corporate.api.model.ProductReserveResult;
import com.corporate.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrderReceiver {

    @Autowired
    OrderService orderService;

    @Autowired
    OrderSender orderSender;

    @JmsListener(destination = Constants.PRODUCT_RESERVE_OK_QUEUE)
    public void receiveProductRecerveOkMessage(final Message<ProductReserveResult> message) {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveProductRecerveOkMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        ProductReserveResult reserveProductResult = message.getPayload();
        orderService.confirmProductReserve(reserveProductResult);
    }

    @JmsListener(destination = Constants.PRODUCT_RESERVE_REJECT_QUEUE)
    public void receiveProductRejectMessage(final Message<ProductReserveResult> message) {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveProductRejectMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        ProductReserveResult reserveProductResult = message.getPayload();
        orderService.rejectProductReserve(reserveProductResult);
    }

    @JmsListener(destination = Constants.CUSTOMER_RESERVE_OK_QUEUE)
    public void receiveCustomerRecerveOkMessage(final Message<CustomerReserveResult> message) {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveCustomerRecerveOkMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        CustomerReserveResult customerReserveResult = message.getPayload();
        orderService.confirmCustomerReserve(customerReserveResult);
    }

    @JmsListener(destination = Constants.CUSTOMER_RESERVE_REJECT_QUEUE)
    public void receiveCustomerRejectMessage( @Payload final CustomerReserveResult customerReserveResult,
                                                @Headers final MessageHeaders headers,
                                                final Message message, final Session session) {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveCustomerRejectMessage(message: {})", customerReserveResult);
        log.info("Application : headers received : {}", headers);
        orderService.rejectCustomerReserve(customerReserveResult);
    }
}