package com.corporate.api.model;

public enum ReserveMoneyStatus {

    RESERVED(0),
    REJECTED(1),
    CONFIRMED(2);

    private int statusId;

    ReserveMoneyStatus(int statusId) {
        this.statusId = statusId;
    }
}
