package com.corporate.api.model.proxy;

import com.corporate.api.model.ProductReserveStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reserve implements Serializable {

    private Long orderId;
    private Long productId;
    private Integer amount;
    private LocalDateTime start;
    private ProductReserveStatus status = ProductReserveStatus.RESERVED;
}
