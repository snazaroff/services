package com.corporate.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Data {
    Object data;
    User owner;

    public Data(Object data, User owner) {
        this.data = data;
        this.owner = owner;
    }
}