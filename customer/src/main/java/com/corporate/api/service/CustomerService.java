package com.corporate.api.service;

import com.corporate.api.configuration.LoggerConfig;
import com.corporate.api.model.*;
//import com.corporate.api.model.proxy.Customer;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.model.proxy.OrderItem;
import com.corporate.api.repository.CustomerRepository;
import com.corporate.api.repository.ReserveMoneyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class CustomerService {

    private LoggerService log;

    private final CustomerRepository customerRepository;
    private final ReserveMoneyRepository reserveMoneyRepository;

    public CustomerService(
            CustomerRepository customerRepository,
            ReserveMoneyRepository reserveMoneyRepository,
            LoggerConfig loggerConfig
    ) {
        this.customerRepository = customerRepository;
        this.reserveMoneyRepository = reserveMoneyRepository;
        log = new LoggerService(CustomerService.class, loggerConfig);
    }

    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Customer getById(Long customerId) {
        return customerRepository.getOne(customerId);
    }

    @Transactional
    public CustomerReserveResult reserve(Order order) {
        log.debug("reserve(order: {})", order);

        int reserveSumma = 0;

        //Считаем сумму по всему заказу
        for(OrderItem orderItem: order.getOrderItems()) {
            reserveSumma = reserveSumma + orderItem.getAmount()*orderItem.getPrice();
        }

        Customer customer = customerRepository.getOne(order.getCustomerId());

        CustomerReserveResult customerReserveResult = new CustomerReserveResult();
        customerReserveResult.setOrderId(order.getOrderId());
        customerReserveResult.setCustomerId(order.getCustomerId());
        customerReserveResult.setAvailableSumma(customer.getDeposit());
        customerReserveResult.setOrderedSumma(reserveSumma);

        if (customer.getDeposit() >= reserveSumma) {
            //Если сумма депозита не меньше суммы заказа
            //резервируем сумму

            customerReserveResult.setCustomerReserveStatus(CustomerReserveStatus.RESERVED);
            ReserveMoney reserveMoney = new ReserveMoney();
            reserveMoney.setOrderId(order.getOrderId());
            reserveMoney.setCustomerId(order.getCustomerId());
            reserveMoney.setStatus(ReserveMoneyStatus.RESERVED);
            reserveMoney.setStart(LocalDateTime.now());
            reserveMoney.setStatus(ReserveMoneyStatus.RESERVED);
            reserveMoney.setSumma(reserveSumma);

            reserveMoneyRepository.save(reserveMoney);

            customer.setDeposit(customer.getDeposit() - reserveSumma);
            customerRepository.save(customer);
        } else {
            customerReserveResult.setCustomerReserveStatus(CustomerReserveStatus.REJECTED);
        }

        return customerReserveResult;
    }

    @Transactional
    public void confirmReserve(@Valid ConfirmReserveOrder confirmReserve) {
        log.debug("confirmReserve(confirmReserve: {})", confirmReserve);
        ReserveMoney reserveMoney = reserveMoneyRepository.findByOrderId(confirmReserve.getOrderId());
        reserveMoney.setStatus(ReserveMoneyStatus.CONFIRMED);
        reserveMoneyRepository.save(reserveMoney);
    }

    @Transactional
    public void rejectReserve(@Valid RejectReserveOrder rejectReserve) {
        log.debug("rejectReserve(rejectReserve: {})", rejectReserve);
        ReserveMoney reserveMoney = reserveMoneyRepository.findByOrderId(rejectReserve.getOrderId());
        if(reserveMoney != null) {
            log.debug("reserveMoney: {}", reserveMoney);
            log.debug("reserveMoney: {}", reserveMoneyRepository.findAll());
            reserveMoney.setStatus(ReserveMoneyStatus.REJECTED);
            reserveMoneyRepository.save(reserveMoney);
            Customer customer = customerRepository.getOne(reserveMoney.getCustomerId());
            customer.setDeposit(customer.getDeposit() + reserveMoney.getSumma());
            customerRepository.save(customer);
        }
    }
}