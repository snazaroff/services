package com.corporate.api.product;

import com.corporate.api.model.Constants;
import com.corporate.api.model.ProductReserveResult;
import com.corporate.api.model.ProductReserveStatus;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.product.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Session;

import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

@Slf4j
@Component
public class ProductReceiver {

    @Autowired
    ProductSender productSender;

    @Autowired
    ProductService productService;

    @JmsListener(destination = Constants.ORDER_RESERVE_TOPIC, containerFactory = "jmsListenerContainerFactory")
//    public void receiveTopicMessage(@Payload Order order,
//                                    @Headers MessageHeaders headers,
//                                    Message message,
//                                    Session session) {
//
//        log.info("received <" + order + ">");
//    }
    public void receiveOrderReserveMessage(final Message<Order> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("receiveOrderReserveMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        Order order = message.getPayload();

        ProductReserveResult productReserveResult = productService.reserve(order);
        if(productReserveResult.getStatus() == ProductReserveStatus.RESERVED)
            productSender.sendProductReserveOk(productReserveResult);
        else
            productSender.sendProductReserveReject(productReserveResult);
    }
}