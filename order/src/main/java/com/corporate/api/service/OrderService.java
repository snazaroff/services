package com.corporate.api.service;

import com.corporate.api.OrderSender;
import com.corporate.api.configuration.LoggerConfig;
import com.corporate.api.model.CustomerReserveResult;
import com.corporate.api.model.Order;
import com.corporate.api.model.ProductReserveResult;
import com.corporate.api.repository.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class OrderService {

    public static final int CANCELLED = 0;
    public static final int CREATED = 1;
    public static final int CUSTOMER_OK = 2;
    public static final int PRODUCT_OK = 4;
    public static final int RESERVED_OK = 7;

    private final OrderRepository orderRepository;
    private final OrderSender orderSender;

    private LoggerService log;

    public final ConcurrentHashMap<Long, Order> orders = new ConcurrentHashMap();

    public OrderService(
            OrderRepository orderRepository,
            OrderSender messageSender,
            LoggerConfig loggerConfig
    ) {
        this.orderRepository = orderRepository;
        this.orderSender = messageSender;
        log = new LoggerService(OrderService.class, loggerConfig);
    }

    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    public Order getById(Long orderId) {
        return orderRepository.getOne(orderId);
    }

    public void reserve(Order order) {
        log.debug("reserve(order: {})", order);

        order.setStatus(CREATED);
        order = orderRepository.save(order);
        synchronized (orders) {
            orders.put(order.getOrderId(), order);
        }
        log.debug("order: {}", order);
        log.debug("order.new: {}", orderRepository.findAll());
        orderSender.sendOrderReserveMessage(order.toProxy());
        while (!(order.getStatus() == CANCELLED ||
                order.getStatus() == RESERVED_OK)) {
            log.debug("order: {}", order);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
            }
        }
        synchronized (orders) {
            orders.remove(order.getOrderId());
        }
    }

    public void reject(Order order) {
        log.debug("reject(order: {})", order);

        order.setStatus(CANCELLED);
        orderRepository.save(order);
        log.debug("order: {}", order);
        log.debug("order.new: {}", orderRepository.findAll());
        orderSender.sendOrderReserveRejectMessage(order.toProxy());
    }

    @Transactional
    public void confirmProductReserve(ProductReserveResult reserveProductResult) {
        log.debug("confirmProductReserve(reserveProductResult: {})", reserveProductResult);
//        Order order = orderRepository.getOne(reserveProductResult.getOrderId());
//        order.setStatus(OrderStatus.or(order.getStatus(), OrderStatus.PRODUCT_OK));
//        order = orderRepository.save(order);
//        if (OrderStatus.RESERVED_OK.equals(order.getStatus())) {
//            //Fix order
//        }
//        synchronized (orders) {
//            log.debug("status: {}", order.getStatus());
//            Order oldOrder = orders.get(order.getOrderId());
//            oldOrder.setStatus(order.getStatus());
//        }
        synchronized (orders) {
            Order order = orders.get(reserveProductResult.getOrderId());
            if (order == null) return;
            log.debug("status: {}", order.getStatus());
            if (CANCELLED != order.getStatus()) {
                order.setStatus(order.getStatus() | PRODUCT_OK);
                log.debug("status: {}", order.getStatus());
                order = orderRepository.save(order);
                if (RESERVED_OK == order.getStatus()) {
                    //Fix order
                }
            }
        }
    }

    @Transactional
    public void rejectProductReserve(ProductReserveResult reserveProductResult) {
        log.debug("rejectProductReserve(reserveProductResult: {})", reserveProductResult);
//        Order order = orderRepository.getOne(reserveProductResult.getOrderId());
//        if (OrderStatus.and(order.getStatus(), OrderStatus.CUSTOMER_OK).equals(OrderStatus.CUSTOMER_OK)) {
//            //нужно откатить customer
//            orderSender.sendOrderReserveRejectMessage(order.toProxy());
//        }
//        order.setStatus(OrderStatus.CANCELLED);
//        orderRepository.save(order);
//        synchronized (orders) {
//            log.debug("status: {}", order.getStatus());
//            Order oldOrder = orders.get(order.getOrderId());
//            oldOrder.setStatus(order.getStatus());
//        }
        synchronized (orders) {
            Order order = orders.get(reserveProductResult.getOrderId());
            if (order == null) return;
            log.debug("status: {}", order.getStatus());
            if (CANCELLED != order.getStatus()) {
                order.setStatus(CANCELLED);
                log.debug("status: {}", order.getStatus());
                orderRepository.save(order);
                //нужно откатить customer
                orderSender.sendOrderReserveRejectMessage(order.toProxy());
            }
        }
    }

    @Transactional
    public void confirmCustomerReserve(CustomerReserveResult customerReserveResult) {
        log.debug("confirmCustomerReserve(reserveProductResult: {})", customerReserveResult);

//        Order order = orderRepository.getOne(customerReserveResult.getOrderId());
//        log.debug("status: {}", order.getStatus());
        synchronized (orders) {
            Order order = orders.get(customerReserveResult.getOrderId());
            if (order == null) return;
            log.debug("status: {}", order.getStatus());
            if (CANCELLED != order.getStatus()) {

//        log.debug("status: {}", order.getStatus());
//                order.setStatus(OrderStatus.or(order.getStatus(), OrderStatus.CUSTOMER_OK));
                order.setStatus(order.getStatus() | CUSTOMER_OK);
                log.debug("status: {}", order.getStatus());
                order = orderRepository.save(order);
                if (RESERVED_OK == order.getStatus()) {
                    //Fix order
                }
//                order.setStatus(order.getStatus());
            }
        }
    }

    @Transactional
    public void rejectCustomerReserve(CustomerReserveResult customerReserveResult) {
        log.debug("rejectCustomerReserve(reserveProductResult: {})", customerReserveResult);
//        Order order = orderRepository.getOne(customerReserveResult.getOrderId());
//        log.debug("order: {}", order);
//        if (OrderStatus.and(order.getStatus(), OrderStatus.CUSTOMER_OK).equals(OrderStatus.CUSTOMER_OK)) {
//            //нужно откатить product
//            orderSender.sendOrderReserveRejectMessage(order.toProxy());
//        }
//        order.setStatus(OrderStatus.CANCELLED);
//        order = orderRepository.save(order);
//        log.debug("order: {}", order);
//        synchronized (orders) {
//            log.debug("status: {}", order.getStatus());
//            Order oldOrder = orders.get(order.getOrderId());
//            oldOrder.setStatus(order.getStatus());
//        }
        synchronized (orders) {
            Order order = orders.get(customerReserveResult.getOrderId());
            if (order == null) return;
            log.debug("status: {}", order.getStatus());
            if (CANCELLED != order.getStatus()) {
                order.setStatus(CANCELLED);
                log.debug("status: {}", order.getStatus());
                orderRepository.save(order);
                //нужно откатить product
                orderSender.sendOrderReserveRejectMessage(order.toProxy());
            }
        }
    }
}