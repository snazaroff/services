package com.corporate.api.service;

import lombok.extern.slf4j.Slf4j;
import org.casbin.jcasbin.main.Enforcer;
import org.springframework.stereotype.Service;

import java.net.URL;

@Slf4j
@Service
public class IdentifyService {

    public IdentifyService() {}

    public boolean identify(String userId, Object data, String action) {
        log.debug("identify(userId: {}, data: {}, action: {})", userId, data, action);

        URL modelUrl = IdentifyService.class.getClassLoader().getResource("abac_model.conf");
        URL policyUrl = IdentifyService.class.getClassLoader().getResource("basic_policy.conf");

//        Enforcer enforcer = new Enforcer("classpath:/resources/abac_model.conf", "classpath:/resources/basic_policy.conf");
        log.debug("modelUrl.getPath(): {}", modelUrl.getPath());
        log.debug("policyUrl.getPath(): {}", modelUrl.getPath());
//        Enforcer enforcer = new Enforcer(modelUrl.getPath(), policyUrl.getPath());

//        Enforcer enforcer = new Enforcer("examples/abac_model.conf", "examples/basic_policy.csv");
        Enforcer enforcer = new Enforcer("examples/priority_model.conf", "examples/priority_policy.csv");

        log.debug("AllObjects: {}", enforcer.getPermissionsForUser("alice"));

        log.debug("AllObjects: {}", enforcer.getAllObjects());

        log.debug("Users: {}", enforcer.getUsersForRole("data2_allow_group"));
        log.debug("Roles: {}", enforcer.getRolesForUser("bob"));

        log.debug("Permissions: {}", enforcer.getPermissionsForUser("bob"));
        log.debug("Permissions: {}", enforcer.getPermissionsForUser("data2_allow_group"));

        return enforcer.enforce(userId, data, action);
    }
}