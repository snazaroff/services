package com.corporate.api.controller;

import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.service.IdentifyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/identify")
@AllArgsConstructor
public class IdentifyController {

    private final IdentifyService identifyService;

//    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<List<Product>> getAll() {
//        log.debug("IdentifyController.getAll()");
//
//        List<Product> products = identifyService.getAll();
//        return new ResponseEntity<>(products, HttpStatus.OK);
//    }
//
    @GetMapping(path = "/{userId}/{data}/{action}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Boolean> identify(@PathVariable String userId, @PathVariable String data, @PathVariable String action) {
        log.debug("IdentifyController.identify(userId: {}, data: {}, action: {})", userId, data, action);

        boolean result = identifyService.identify(userId, data, action);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

//    @PostMapping(path = "/confirm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<Void> confirmReserve(@RequestBody @Valid ConfirmReserveOrder confirmReserve) {
//        log.debug("IdentifyController.confirmReserve(confirmReserve: {})", confirmReserve);
//
//        identifyService.confirmReserve(confirmReserve);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
//
//    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<ProductReserveResult> reserve(@RequestBody @Valid Order order) {
//        log.debug("IdentifyController.reserve(order: {})", order);
//
//        ProductReserveResult productReserveResult = identifyService.reserve(order);
//        return new ResponseEntity<>(productReserveResult, HttpStatus.OK);
//    }
//
//    @PostMapping(path = "/reject", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<Void> rejectReserve(@RequestBody @Valid RejectReserveOrder rejectReserve) {
//        log.debug("IdentifyController.rejectReserve(confirmReserve: {})", rejectReserve);
//
//        identifyService.rejectReserve(rejectReserve);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

}