package com.corporate.api.service;

import com.corporate.api.OrderSender;
import com.corporate.api.model.Order;
import com.corporate.api.model.ProductReserveResult;
import com.corporate.api.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ProductServiceAnother {

    private final OrderRepository orderRepository;
    private final OrderSender messageSender;

    public ProductServiceAnother(
            OrderSender messageSender,
            OrderRepository orderRepository
    ) {
        this.messageSender = messageSender;
        this.orderRepository = orderRepository;
    }

    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    public Order getById(Long orderId) {
        return orderRepository.getOne(orderId);
    }

    public List<ProductReserveResult> reserve(Order order) {
        log.debug("reserve(order: {})", order);
//        order = orderRepository.save(order);
        orderRepository.save(order);
        log.debug("order: {}", order);
        log.debug("order.new: {}", orderRepository.findAll());
//        ArrayList<Reserve> reserves = new ArrayList<>();
//        for(OrderItem item: order.getOrderItems()) {
//            Reserve reserve = new Reserve();
//            reserve.setOrderId(order.getOrderId());
//            reserve.setAmount(reserve.getAmount());
//            reserve.setProductId(item.getProductId());
//            reserves.add(reserve);
//        }
        messageSender.sendOrderReserveRejectMessage(order.toProxy());
        return null;
    }
}