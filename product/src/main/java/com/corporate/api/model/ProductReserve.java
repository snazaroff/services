package com.corporate.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name="product_reserve")
public class ProductReserve implements Serializable {

    @Id
    @Column(name = "reserve_id")
    @JsonProperty("reserve_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reserveId;
    @Column(name = "order_id")
    private Long orderId;
    @Column(name = "product_id")
    private Long productId;
    @Column(name = "amount")
    private Integer amount;
    @Column(name = "start_date_time")
    private LocalDateTime start;
    @Column(name = "status")
    private ProductReserveStatus status;

    public ProductReserve() { }

    public ProductReserve(Long orderId, ProductReserveItem productReserveItem) {
        this.orderId = orderId;
        this.productId = productReserveItem.getProductId();
        this.amount = productReserveItem.getOrderedAmount();
        this.start = LocalDateTime.now();
        this.status = ProductReserveStatus.RESERVED;
    }
}
