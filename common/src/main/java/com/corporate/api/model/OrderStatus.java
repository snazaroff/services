package com.corporate.api.model;

public enum OrderStatus {

    INIT(0);
//    CANCELLED(0),
//    CREATED(1),
//    CUSTOMER_OK(2),
//    PRODUCT_OK(4),
//    RESERVED_OK(7);

    private int statusId;

    OrderStatus(int statusId) {
        this.statusId = statusId;
    }

    public static OrderStatus or(OrderStatus os1, OrderStatus os2) {
        if(os1 == null)
            return os2;
        else if(os2 == null)
            return os1;
        else {
            int value = os1.statusId | os2.statusId;
            return getByStatusId(value);
        }
    }

    public static OrderStatus and(OrderStatus os1, OrderStatus os2) {
        if(os1 == null)
            return os2;
        else if(os2 == null)
            return os1;
        else {
            int value = os1.statusId & os2.statusId;
            return getByStatusId(value);
        }
    }

    private static OrderStatus getByStatusId(int statusId) {
        for(OrderStatus orderStatus: values()) {
            if(orderStatus.statusId == statusId)
                return orderStatus;
        }
        return null;
    }

}
