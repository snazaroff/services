package com.corporate.api.model;

public enum ReserveOrderStatus {

    RESERVED(0),
    REJECTED(1),
    CONFIRMED(2);

    private int statusId;

    ReserveOrderStatus(int statusId) {
        this.statusId = statusId;
    }
}
