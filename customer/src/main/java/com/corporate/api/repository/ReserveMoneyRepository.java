package com.corporate.api.repository;

import com.corporate.api.model.ReserveMoney;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReserveMoneyRepository extends JpaRepository<ReserveMoney, Long> {
    ReserveMoney findByOrderId(Long orderId);
}