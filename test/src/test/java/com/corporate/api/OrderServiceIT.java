package com.corporate.api;

import com.corporate.api.model.*;
import com.corporate.api.service.CustomerService;
import com.corporate.api.service.OrderService;
import com.corporate.api.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderApplication.class)
@Slf4j
@WebAppConfiguration
@Sql(value = {"script.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"clear.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class OrderServiceIT {

    private MockMvc mockMvc;

    private ObjectMapper om = new ObjectMapper();

    private List<Product> products = Collections.emptyList();

    private List<Customer> customers = Collections.emptyList();

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    CustomerService customerService;

    @Autowired
    OrderService orderService;

    @Autowired
    ProductService productService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        customers = customerService.getAll();
        products = productService.getAll();
    }

    @Test
//    @Ignore
    public void testRejectCustomerReserve() throws Exception {
        log.debug("testRejectCustomerReserve()");

        Customer customer = customers.get(0);
        Order order = new Order();
        order.setCustomerId(customer.getCustomerId());
        for (Product product : products) {
            OrderItem orderItem = new OrderItem();
            orderItem.setProductId(product.getProductId());
            orderItem.setPrice(product.getPrice());
            orderItem.setAmount(100);
            orderItem.setOrder(order);
            order.getOrderItems().add(orderItem);
        }

        long startTime = System.currentTimeMillis();
        orderService.reserve(order);
        log.debug("reserve finished!: {} ms", System.currentTimeMillis() - startTime);

        Assert.assertEquals(OrderService.CANCELLED, order.getStatus().intValue());
        log.debug("Result:");

        List<Order> orders = orderService.getAll();
        log.debug("orders: {}", orders.size());
        for (Order ord : orders) {
            log.debug("order: {}", ord);
        }

        List<Product> products = productService.getAll();
        log.debug("products: {}", products.size());
        for (Product product : products) {
            log.debug("product: {}", product);
        }

        List<Customer> customers = customerService.getAll();
        log.debug("customers: {}", customers.size());
        for (Customer cust : customers) {
            log.debug("customer: {}", cust);
        }
    }

    @Test
//    @Ignore
    public void testReserveOk() throws Exception {
        log.debug("testReserveOk()");

        Customer customer = customers.get(1);
        Order order = new Order();
        order.setCustomerId(customer.getCustomerId());
        for (Product product : products) {
            OrderItem orderItem = new OrderItem();
            orderItem.setProductId(product.getProductId());
            orderItem.setPrice(product.getPrice());
            orderItem.setAmount(1);
            orderItem.setOrder(order);
            order.getOrderItems().add(orderItem);
        }

        orderService.reserve(order);

        Assert.assertEquals(OrderService.RESERVED_OK, order.getStatus().intValue());

        log.debug("Result:");

        List<Order> orders = orderService.getAll();
        log.debug("orders: {}", orders.size());
        log.debug(om.writeValueAsString(orders));
        for (Order ord : orders) {
            log.debug("order: {}", ord);
        }

        List<Product> products = productService.getAll();
        log.debug("products: {}", products.size());
        for (Product product : products) {
            log.debug("product: {}", product);
        }

        List<Customer> customers = customerService.getAll();
        log.debug("customers: {}", customers.size());
        for (Customer cust : customers) {
            log.debug("customer: {}", cust);
        }
    }
}