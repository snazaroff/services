package com.corporate.api.product.service;

//import com.corporate.api.model.*;
import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.model.proxy.OrderItem;
import com.corporate.api.product.model.ProductReserve;
import com.corporate.api.product.repository.ProductRepository;
import com.corporate.api.product.repository.ProductReserveRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductReserveRepository reserveRepository;

        public ProductService(
            ProductRepository productRepository,
            ProductReserveRepository reserveRepository) {
        this.productRepository = productRepository;
        this.reserveRepository = reserveRepository;
    }

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public Product getById(Long productId) {
        return productRepository.getOne(productId);
    }

    @Transactional
    public ProductReserveResult reserve(Order order) {
        log.debug("reserve(order: {})", order);
        ProductReserveResult result = new ProductReserveResult();
        result.setOrderId(order.getOrderId());

        for(OrderItem orderItem: order.getOrderItems()) {
            Product product = productRepository.getOne(orderItem.getProductId());

            ProductReserveItem productReserveItem = new ProductReserveItem();
            productReserveItem.setAvailableAmount(product.getAmount());
            productReserveItem.setOrderedAmount(orderItem.getAmount());
            productReserveItem.setProductId(product.getProductId());

            result.getProductReserveItems().add(productReserveItem);
        }
        //Проверяем наличие достаточного кол-ва товара
        if(result.isOk()) {
            //Сохраняем
            result.setStatus(ProductReserveStatus.RESERVED);
            for(ProductReserveItem productReserveItem: result.getProductReserveItems()) {
                ProductReserve reserveProduct = new ProductReserve();
                reserveProduct.setReserveId(0L);
                reserveProduct.setOrderId(order.getOrderId());
                reserveProduct.setProductId(productReserveItem.getProductId());
                reserveProduct.setAmount(productReserveItem.getOrderedAmount());
                reserveProduct.setStart(LocalDateTime.now());
                reserveProduct.setStatus(ProductReserveStatus.RESERVED);

                Product product = productRepository.getOne(reserveProduct.getProductId());
                if(product.getAmount() >= productReserveItem.getOrderedAmount()) {
                    product.setAmount(product.getAmount() - productReserveItem.getOrderedAmount());
                    log.debug("product: {}", product);
                    productRepository.save(product);
                }
                reserveRepository.save(reserveProduct);
            }

        } else {
            result.setStatus(ProductReserveStatus.REJECT);
        }
        return result;
    }

    @Transactional
    public void confirmReserve(@Valid ConfirmReserveOrder confirmReserve) {
        log.debug("confirmReserve(confirmReserve: {})", confirmReserve);
        List<ProductReserve> reserves = reserveRepository.findByOrderId(confirmReserve.getOrderId());
        for(ProductReserve reserve: reserves) {
            reserve.setStatus(ProductReserveStatus.CONFIRMED);
        }
        reserveRepository.saveAll(reserves);
    }

    @Transactional
    public void rejectReserve(@Valid RejectReserveOrder rejectReserve) {
        log.debug("rejectReserve(rejectReserve: {})", rejectReserve);
        List<ProductReserve> reserves = reserveRepository.findByOrderId(rejectReserve.getOrderId());
        for(ProductReserve reserve: reserves) {
            reserve.setStatus(ProductReserveStatus.REJECT);
            Product product = productRepository.getOne(reserve.getProductId());
            product.setAmount(product.getAmount() + reserve.getAmount());
            productRepository.save(product);
        }
        reserveRepository.saveAll(reserves);
    }
}