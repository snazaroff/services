package com.corporate.api.customer.repository;

import com.corporate.api.customer.model.ReserveMoney;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MoneyReserveRepository {

    private final List<ReserveMoney> reserveProducts = new ArrayList<>();

    public ReserveMoney getOne(Long reserveProductId) {
        if(reserveProductId == null || reserveProductId.equals(""))
            return null;
        for(ReserveMoney reserveProduct: reserveProducts) {
            if(reserveProductId.equals(reserveProduct.getReserveId()))
                return reserveProduct;
        }
        return null;
    }

    public List<ReserveMoney> findByOrderId(final Long orderId) {
        if(orderId == null || orderId.equals(""))
            return null;

        return reserveProducts.stream().filter(p->orderId.equals(p.getOrderId())).collect(Collectors.toList());
    };

    public void save(ReserveMoney reserveMoney) {
        ReserveMoney oldReserveMoney = getOne(reserveMoney.getReserveId());
        if(oldReserveMoney != null) {
            oldReserveMoney.setOrderId(reserveMoney.getOrderId());
            oldReserveMoney.setSumma(reserveMoney.getSumma());
            oldReserveMoney.setStatus(reserveMoney.getStatus());
        } else
            reserveProducts.add(reserveMoney);
    }

    public void saveAll(List<ReserveMoney> reserves) {
        for(ReserveMoney reserveProduct: reserves) {
            save(reserveProduct);
        }
    }
}