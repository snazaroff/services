package com.corporate.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ToString
@AllArgsConstructor
public class RejectReserveOrder implements Serializable {
    @NotNull
    private Long orderId;
}
