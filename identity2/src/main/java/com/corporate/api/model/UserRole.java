package com.corporate.api.model;

public enum UserRole {
	ADMIN, PM, DEVELOPER, TESTER
}
