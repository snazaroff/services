package com.corporate.api.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class AbacSecurityInitializer extends AbstractSecurityWebApplicationInitializer {}
