package com.corporate.api;

import com.corporate.api.model.Data;
import com.corporate.api.model.Department;
import com.corporate.api.model.Role;
import com.corporate.api.model.User;
import com.corporate.api.service.IdentifyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class Identity2ServiceIT {

    private MockMvc mockMvc;

    private ObjectMapper om = new ObjectMapper();

    @Autowired
    IdentifyService identifyService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }


    @Test
//    @Ignore
    public void test() throws Exception {
        log.debug("test()");

        Department mainDepartment = new Department("MainDepartment");
        Department dep_1 = new Department("Department#1");
        Department dep_2 = new Department("Department#2");


        User vasya = new User("Vasya", mainDepartment, new Role("ADMIN"));
        User alice = new User("Alice", dep_1, new Role("BOSS"));
        User bob = new User("Bob", dep_1, new Role("MANAGER"));
        bob.setBoss(alice);
        User petya = new User("Petya", dep_1, new Role("BOSS"));
        petya.setBoss(alice);

        Data aliceData = new Data("data1", alice);
        Data bobData = new Data("data2", bob);

        TestData[] testData = {
                new TestData(vasya, aliceData,  "read"),
                new TestData(vasya, aliceData,  "write"),
                new TestData(alice, aliceData,  "read"),
                new TestData(alice, aliceData,  "write"),
                new TestData(alice, bobData,    "read"),
                new TestData(alice, bobData,    "write"),
                new TestData(bob,   aliceData,  "read"),
                new TestData(bob,   aliceData,  "write"),
                new TestData(bob,   bobData,    "read"),
                new TestData(bob,   bobData,    "write"),
                new TestData(petya, bobData,    "read"),
                new TestData(petya, bobData,    "write")
        };

        Map<Object, Boolean> resultList = new TreeMap<>();
        for (TestData item : testData) {
            Boolean result = identifyService.identify(item.user, item.data, item.action);
            resultList.put(item, result);
        }
        for (Map.Entry<Object, Boolean> entry : resultList.entrySet()) {
            log.debug("item: {}: {}", om.writeValueAsString(entry.getKey()), entry.getValue());
        }

    }

    @Setter
    @Getter
    @ToString
    static class TestData implements Comparable<TestData> {
        User user;
        Data data;
        String action;

        public TestData(User user, Data data, String action) {
            this.user = user;
            this.data = data;
            this.action = action;
        }

        @Override
        public int compareTo(TestData o) {
            if (this.user.getName().compareTo(o.user.getName()) == 0) {
                if (this.data.getOwner().getName().compareTo(o.data.getOwner().getName()) == 0) {
                        return this.action.compareTo(o.action);
                } else {
                    return this.data.getOwner().getName().compareTo(o.data.getOwner().getName());
                }
            } else {
                return this.user.getName().compareTo(o.user.getName());
            }
        }
    }

}