package com.corporate.api.model.proxy;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@ToString
public class Order implements Serializable {

    @NotNull
    private Long orderId;
    @NotNull
    private Long customerId;
    @NotEmpty
    Set<OrderItem> orderItems = new HashSet<>();

    public Order(Long orderId, Long customerId) {
        this.orderId = orderId;
        this.customerId = customerId;
    }
}
