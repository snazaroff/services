package com.corporate.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class ProductReserveItem implements Serializable {
    private Long productId;
    private Integer orderedAmount;
    private Integer availableAmount;

    @JsonIgnore
    public boolean isOk() {
        return availableAmount >= orderedAmount;
    }
}
