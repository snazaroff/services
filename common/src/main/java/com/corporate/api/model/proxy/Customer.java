package com.corporate.api.model.proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class Customer {

    public Customer() { }

    @NotNull
    private Long customerId;

    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Deposit cannot be null")
    private Integer deposit = 0;
}