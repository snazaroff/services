package com.corporate.api;

import javax.jms.*;

import com.corporate.api.model.Constants;
import com.corporate.api.model.RejectReserveOrder;
import com.corporate.api.model.proxy.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Slf4j
@Component
public class OrderSender {

    private static final Boolean NON_TRANSACTED = false;

    private final JmsTemplate jmsTemplate;

    public OrderSender(JmsTemplate jmsTemplate) throws JMSException {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendOrderReserveMessage(final Order order) {
        log.debug("sendOrderReserveMessage(order: {})", order);

        jmsTemplate.convertAndSend(Constants.ORDER_RESERVE_TOPIC, order);

        sendToReport(order);
    }

    public void sendOrderReserveRejectMessage(final Order order) {
        log.debug("sendOrderReserveRejectMessage(order: {})", order);

        jmsTemplate.send(Constants.ORDER_RESERVE_REJECT_QUEUE, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                ObjectMessage objectMessage = session.createObjectMessage(new RejectReserveOrder(order.getOrderId()));
                return objectMessage;
            }
        });

        sendToReport(new RejectReserveOrder(order.getOrderId()));
    }

    private void sendToReport(Serializable msg) {
        jmsTemplate.send(Constants.REPORT_QUEUE, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                ObjectMessage objectMessage = session.createObjectMessage(msg);
                return objectMessage;
            }
        });
    }
}