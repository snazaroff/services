INSERT INTO customer(NAME, AMOUNT)
VALUES('Волобуев Аристарх Эдуардович', 1000);

INSERT INTO customer(NAME, AMOUNT)
VALUES('Здубов-Пердунов Василий Парамонович', 1500);

INSERT INTO customer(NAME, AMOUNT)
VALUES('Свинтокрылова Пелагея Марфовна', 30);

INSERT INTO orders()
VALUES();

SELECT SET(@order_id, IDENTITY()) RUNNING_TOTAL FROM SYSTEM_RANGE(1, 1);

INSERT INTO order_item( ORDER_ID, PRODUCT_ID, AMOUNT)
VALUES(@order_id, 1, 10);

INSERT INTO order_item( ORDER_ID, PRODUCT_ID, AMOUNT)
VALUES(@order_id, 2, 20);

INSERT INTO product(name, price, amount)
VALUES('Мясорубка', 25, 10);

INSERT INTO product(name, price, amount)
VALUES('Чапельник', 3, 15);

INSERT INTO product(name, price, amount)
VALUES('Розетка', 1, 3);

