package com.corporate.api;

import com.corporate.api.model.*;
import com.corporate.api.service.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;

@Slf4j
@Component
public class ReportReceiver {

    @Autowired
    ReportSender productSender;

    @Autowired
    ReportService reportService;

    @JmsListener(destination = Constants.REPORT_QUEUE, containerFactory = "jmsListenerContainerFactory")
    public void reportMessage(final Message<String> message) throws JMSException {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("reportMessage(message: {})", message.getPayload());
        MessageHeaders headers =  message.getHeaders();
        log.info("Application : headers received : {}", headers);

        String report = message.getPayload();

        reportService.add(report);
    }
}