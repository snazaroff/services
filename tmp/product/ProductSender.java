package com.corporate.api.product;

import com.corporate.api.model.Constants;
import com.corporate.api.model.ProductReserveResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.util.ArrayList;

@Slf4j
@Component
public class ProductSender {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendProductReserveOk(final ProductReserveResult productReserveResult) {
        log.debug("sendProductReserveOk: {}", productReserveResult);

        jmsTemplate.send(Constants.PRODUCT_RESERVE_OK_QUEUE, new MessageCreator(){
            @Override
            public Message createMessage(Session session) throws JMSException{
                ObjectMessage objectMessage = session.createObjectMessage(productReserveResult);
                return objectMessage;
            }
        });
    }

    public void sendProductReserveReject(final ProductReserveResult productReserveResult) {
        log.debug("sendProductReserveReject: {}", productReserveResult);

        jmsTemplate.send(Constants.PRODUCT_RESERVE_REJECT_QUEUE, new MessageCreator(){
            @Override
            public Message createMessage(Session session) throws JMSException{
                ObjectMessage objectMessage = session.createObjectMessage(productReserveResult);
                return objectMessage;
            }
        });
    }
}

