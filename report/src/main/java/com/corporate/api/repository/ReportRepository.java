package com.corporate.api.repository;

import com.corporate.api.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Product, Long> {
//      Product getByPhone(String phone);
}