package com.corporate.api;

import com.corporate.api.model.*;
import com.corporate.api.model.Product;
import com.corporate.api.model.proxy.*;
import com.corporate.api.repository.*;
import com.corporate.api.service.ReportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class ProductServiceIT {

    private MockMvc mockMvc;

    private ObjectMapper om = new ObjectMapper();

    private final Customer[] customers = {new Customer(1L, "Волобуев", 30), new Customer(2L, "Прошкин", 1000)};

    @Autowired
    ReportService productService;

    @Autowired
    ReportRepository productRepository;

    @Autowired
    ProductReserveRepository reserveRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }


    @Test
//    @Ignore
    public void test() throws Exception {
        log.debug("test()");


    }
}