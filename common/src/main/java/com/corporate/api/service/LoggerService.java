package com.corporate.api.service;

import com.corporate.api.configuration.LoggerConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerService {

    private LoggerConfig loggerConfig;

    private final Logger log;

    private final ObjectMapper om = new ObjectMapper();

    public LoggerService(Class owner, LoggerConfig loggerConfig) {
        this.loggerConfig = loggerConfig;
        log = LoggerFactory.getLogger(owner);
        if(loggerConfig.isPretty())
            om.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public void debug(String msg, Object ... objs) {
        try {
            if(loggerConfig.isFormated()) {
                for(int i = 0; i < objs.length; i++)
                    objs[i] = om.writeValueAsString(objs[i]);
            }
            log.debug(msg, objs);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void error(String msg, Throwable t) {
        log.error(msg, t);
    }
}
