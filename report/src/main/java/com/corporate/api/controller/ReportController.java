package com.corporate.api.controller;

import com.corporate.api.model.*;
import com.corporate.api.model.proxy.Order;
import com.corporate.api.service.ReportService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/product")
@AllArgsConstructor
public class ReportController {

    private final ReportService productService;

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<Product>> getAll() {
        log.debug("ReportController.getAll()");

        List<Product> products = productService.getAll();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping(path = "/{productId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Product> get(@PathVariable Long productId) {
        log.debug("ReportController.info(productId: {})", productId);

        Product product = productService.getById(productId);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

//    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<ProductReserveResult> reserve(@RequestBody @Valid Order order) {
//        log.debug("ReportController.reserve(order: {})", order);
//
//        ProductReserveResult productReserveResult = productService.reserve(order);
//        return new ResponseEntity<>(productReserveResult, HttpStatus.OK);
//    }
//
//    @PostMapping(path = "/confirm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<Void> confirmReserve(@RequestBody @Valid ConfirmReserveOrder confirmReserve) {
//        log.debug("ReportController.confirmReserve(confirmReserve: {})", confirmReserve);
//
//        productService.confirmReserve(confirmReserve);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
//
//    @PostMapping(path = "/reject", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    ResponseEntity<Void> rejectReserve(@RequestBody @Valid RejectReserveOrder rejectReserve) {
//        log.debug("ReportController.rejectReserve(confirmReserve: {})", rejectReserve);
//
//        productService.rejectReserve(rejectReserve);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

}