package com.corporate.api.customer.model;

import com.corporate.api.model.ReserveMoneyStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReserveMoney implements Serializable {

    private Long reserveId;
    private Long orderId;
    private Long customerId;
    private Integer summa;
    private LocalDateTime start = LocalDateTime.now();
    private ReserveMoneyStatus status;

    public ReserveMoney() { }
}
