package com.corporate.api.model.proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class Product {

    public Product() { }

    @NotNull
    private Long productId;

    @NotNull(message = "Name cannot be null")
    private String name;

    @NotBlank(message = "Price cannot be null")
    private Integer price;

    @NotBlank(message = "Amount cannot be null")
    private Integer amount;
}