package com.corporate.api.product.repository;

import com.corporate.api.product.model.ProductReserve;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ProductReserveRepository {

    private final List<ProductReserve> reserveProducts = new ArrayList<>();

    public ProductReserve getOne(Long reserveProductId) {
        if(reserveProductId == null || reserveProductId.equals(""))
            return null;
        for(ProductReserve reserveProduct: reserveProducts) {
            if(reserveProductId.equals(reserveProduct.getReserveId()))
                return reserveProduct;
        }
        return null;
    }

    public List<ProductReserve> findByOrderId(final Long orderId) {
        if(orderId == null || orderId.equals(""))
            return null;

        return reserveProducts.stream().filter(p->orderId.equals(p.getOrderId())).collect(Collectors.toList());
    };

    public void save(ProductReserve reserveProduct) {
        ProductReserve oldReserveProduct = getOne(reserveProduct.getReserveId());
        if(oldReserveProduct != null) {
            oldReserveProduct.setOrderId(reserveProduct.getOrderId());
            oldReserveProduct.setAmount(reserveProduct.getAmount());
            oldReserveProduct.setStatus(reserveProduct.getStatus());
        } else
            reserveProducts.add(reserveProduct);
    }

    public void saveAll(List<ProductReserve> reserves) {
        for(ProductReserve reserveProduct: reserves) {
            save(reserveProduct);
        }
    }
}