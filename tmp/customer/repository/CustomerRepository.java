package com.corporate.api.customer.repository;

import com.corporate.api.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class CustomerRepository {

    private final Customer[] customers = {
            new Customer(1L, "Волобуеав", 300),
            new Customer(2L, "Гунько", 5000)};

    public List<Customer> findAll() {
        return Arrays.asList(customers);
    }

    public Customer getOne(Long productId) {
        if(productId == null || productId.equals(""))
            return null;
        for(Customer customer: customers) {
            if(productId.equals(customer.getCustomerId()))
                return customer;
        }
        return null;
    }

    public void save(Customer customer) {
        Customer oldCustomer = getOne(customer.getCustomerId());
        if(oldCustomer != null) {
            oldCustomer.setName(customer.getName());
            oldCustomer.setDeposit(customer.getDeposit());
        }
    }
}