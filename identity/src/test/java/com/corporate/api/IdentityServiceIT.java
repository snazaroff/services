package com.corporate.api;

import com.corporate.api.model.proxy.*;
import com.corporate.api.service.IdentifyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class IdentityServiceIT {

    private MockMvc mockMvc;

    private ObjectMapper om = new ObjectMapper();

    private final Customer[] customers = {new Customer(1L, "Волобуев", 30), new Customer(2L, "Прошкин", 1000)};

    @Autowired
    IdentifyService identifyService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }


    @Test
//    @Ignore
    public void test() throws Exception {
        log.debug("test()");


        Object[][] testData = {
                {"alice", new Data("data1", "alice"), "read"},
                {"bob", new Data("data1", "alice"), "data2", "write"}};
        for (Object[] item : testData) {
            Boolean result = identifyService.identify((String)item[0], item[1], (String)item[2]);
            log.debug("item: {}: {}", item, result);
        }
    }


    @Setter
    @Getter
    static class Data {
        Object data;
        Object owner;

        public Data(Object data, Object owner) {
            this.data = data;
            this.owner = owner;
        }
    }
}