package com.corporate.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@AllArgsConstructor
public class ConfirmReserveOrder {
    @NotNull
    private Long orderId;
}
