package com.corporate.api;

import com.corporate.api.model.Constants;
import com.corporate.api.model.CustomerReserveResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

@Slf4j
@Component
public class CustomerSender {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendCustomerReserveOk(final CustomerReserveResult customerReserveResult) {
        log.debug("sendCustomerReserveOk: {}", customerReserveResult);

        jmsTemplate.send(Constants.CUSTOMER_RESERVE_OK_QUEUE, new MessageCreator(){
            @Override
            public Message createMessage(Session session) throws JMSException{
                ObjectMessage objectMessage = session.createObjectMessage(customerReserveResult);
                return objectMessage;
            }
        });
    }

    public void sendCustomerReserveReject(final CustomerReserveResult customerReserveResult) {
        log.debug("sendCustomerReserveReject: {}", customerReserveResult);

        jmsTemplate.send(Constants.CUSTOMER_RESERVE_REJECT_QUEUE, new MessageCreator(){
            @Override
            public Message createMessage(Session session) throws JMSException{
                ObjectMessage objectMessage = session.createObjectMessage(customerReserveResult);
                return objectMessage;
            }
        });
    }
}