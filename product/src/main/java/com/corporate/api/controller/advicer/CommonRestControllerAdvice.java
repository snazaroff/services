package com.corporate.api.controller.advicer;

import com.corporate.api.exception.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CommonRestControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler({HttpClientErrorException.class })
    protected ResponseEntity<Object> handleHttpClientErrorException(HttpClientErrorException ex, WebRequest request) {
        return handleExceptionInternal(ex,  ex.getResponseBodyAsString(), new HttpHeaders(), ex.getStatusCode(), request);
    }

    @ExceptionHandler({HttpServerErrorException.class })
    protected ResponseEntity<Object> handleHttpServerErrorException(HttpServerErrorException ex, WebRequest request) {
        return handleExceptionInternal(ex,  ex.getResponseBodyAsString(), new HttpHeaders(), ex.getStatusCode(), request);
    }


    @ExceptionHandler({WrongRegistrationCodeException.class })
    protected ResponseEntity<Object> handleWrongRegistrationCodeException(WrongRegistrationCodeException ex, WebRequest request) {
        return handleExceptionInternal(ex,  ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
