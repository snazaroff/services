package com.corporate.api.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
public class LoggerConfig {

    @Value("${logger.formated:false}")
    private boolean formated;
    @Value("${logger.pretty:false}")
    private boolean pretty;
}
