package com.corporate.api.service;

import java.util.List;

import com.corporate.api.model.Issue;
import com.corporate.api.model.Project;

public interface IssueService {
	public List<Issue> getIssues(Project project);
	public Issue getIssue(Integer id);
	public void createIssue(Issue issue);
	public void updateIssue(Issue issue);
	public void deleteIssue(Issue issue);
}
