package com.corporate.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
public class ProductReserveResult implements Serializable {
    private Long orderId;
    private List<ProductReserveItem> productReserveItems = new ArrayList<>();
    private ProductReserveStatus status;

    @JsonIgnore
    public boolean isOk() {
        return !productReserveItems
                .stream()
                .filter(f->!f.isOk())
                .findFirst()
                .isPresent();
    }
}
